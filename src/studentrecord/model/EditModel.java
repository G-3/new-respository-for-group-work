/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentrecord.model;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author Life without stress
 */
public class EditModel {
    
    public void edit(student s){
        try
        { 
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/records?user=root&password=grace@50");
            PreparedStatement p = conn.prepareStatement("Update student set studentID=?, surname=?, `first name`=?,"
                    + " `admission year`=?, GPA=?, program=? Where studentID =?");
            p.setString(1, s.studentID);
            p.setString(2, s.surname);
            p.setString(3, s.firstName);
            p.setInt(4, s.admissionYear);
            p.setFloat(5, s.GPA);
            p.setString(6, s.getMajor());
            p.setString(7, s.studentID);
            p.executeUpdate();
            p.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}

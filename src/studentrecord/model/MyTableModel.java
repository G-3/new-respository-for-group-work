/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package studentrecord.model;

import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Samuel Gu
 */
public class MyTableModel extends AbstractTableModel {
    private final ArrayList<student> arr;
    static MyTableModel theModel = null;
    String[] columnNames = null;
    
    

    public MyTableModel() {
        super();     
        arr = new ArrayList<student>();
        fetchTableData(); //my own function to initialize the araylist
    }
    
    public static MyTableModel getInstance(){     //I am using the Singleton pattern here
        if (theModel == null)
            theModel = new MyTableModel();
        return theModel;
    }
    
    @Override
    public int getColumnCount() {        
        return columnNames.length; //we know student has 4 fields
    }

    @Override
    public int getRowCount() {
        return arr.size();
    }

    @Override
    public Object getValueAt(int row, int column) {
       student nStudent = arr.get(row);
        if (column == 0)
            return nStudent.studentID;
        else if (column == 1)
            return nStudent.surname;
        else if (column == 2)
            return nStudent.firstName;
        else if (column == 3)
            return nStudent.admissionYear;
        else if (column == 4)
            return nStudent.GPA;
        else if (column == 5)
            return nStudent.program.toString();
        else
            return null; 
     
    }//fxn getValueAt
    
    
    //optional functions
  
    @Override
    public String getColumnName(int col) {
        return columnNames[col];//otherwise returns A, B, C etc
    }
    //overide this to allow editing of some columns/rows

    @Override
    public boolean isCellEditable(int row, int col) {
	return false;
    }
        
    // uses this to determine the default renderer or 
    // editor for each cell. 
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
   
    //implement this to be able to alter your table data from UI!
    //without this, all edits are discarded.
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        student row = arr.get(rowIndex);
        String key = row.studentID;
        if(0 == columnIndex) {
            row.studentID = (String) aValue ;
        }
        else if(1 == columnIndex) {
        }
        else if(2 == columnIndex) {
            row.firstName = (String) aValue;
        }
        else if(3 == columnIndex) {
            row.admissionYear = (int) aValue;
        }
        else if(4 == columnIndex) {
            row.GPA = (float) aValue;
        }
        else if(5 == columnIndex) {
            String m = (String) aValue;
            for (Major p : Major.values()) {
                if(m.equals(p.toString()))
                {
                    row.program = p;
                    break;
                }
            } 
        }
	
        fireTableCellUpdated(rowIndex, columnIndex); //*works best with this

    }
    
    //my own function to add to model
    //this could have received the raw data as four params and added.
    /*
    public void addToModel(Student s){
        data.add(s);        
        fireTableRowsInserted(0, 0);
    }
    public ArrayList<Student> getList(){
        return data;
    }
    */
    
    void fetchTableData(){
    try {
        Connection conn = null;
	Class.forName("com.mysql.jdbc.Driver").newInstance();
	conn = java.sql.DriverManager.getConnection(
	"jdbc:mysql://localhost/records?user=root&password=grace@50");
	
        Statement s = conn.createStatement();
        
        ResultSet rs = s.executeQuery("SELECT studentID, surname, `first name`, `admission year`, GPA, program FROM student");
        ResultSetMetaData meta = rs.getMetaData();
        int numberOfColumns = meta.getColumnCount();
        columnNames = new String[numberOfColumns];
        for (int i = 1; i <= numberOfColumns; i++) { //note its 1 based.
            columnNames[i - 1] = meta.getColumnName(i); //columns Zero based.
        }
        
        ArrayList <Object> rows = new ArrayList<Object>();
        //get actual row data
        
        while(rs.next()) {
            student st = new student(rs.getString(1), rs.getString(2), rs.getString(3),
                    Integer.parseInt(rs.getString(4)),Float.parseFloat(rs.getString(5)), rs.getString(6));
            arr.add(st);
        }
    }
    catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            System.exit(0);
    }	

        
    }
    
}

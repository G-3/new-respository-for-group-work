/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentrecord.model;

/**
 *
 * @author User
 */
public class student implements Comparable<student> {
     String studentID;
        String surname;
	String firstName;
	int admissionYear;
	float GPA;
	Major program;
        
        public student(String id, String su, String f, int ad, float g, String m)
        {
            studentID = id;
            surname = su;
            firstName = f;
            admissionYear = ad;
            GPA = g;
            for (Major p : Major.values()) {
                if(m.equals(p.toString()))
                {
                    program = p;
                    break;
                }
            } 
        }
        @Override
	public String toString()
	{
		return  studentID
		+ "," + surname + "," + firstName
		+ "," +admissionYear +
		"," + GPA +
		"," + program;
	}
        @Override
        public int compareTo(student s)
	{
		return this.firstName.compareTo(s.firstName);

	}
        public String getName()
        { 
            return firstName + " " + surname;
        }
        public String getMajor()
        {
            return program.toString();
        }
}
enum Major {BA, CS, MIS, EE, CE, ME}
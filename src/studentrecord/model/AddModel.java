/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentrecord.model;

import java.sql.Connection;
import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author Mac-Noble
 */
public class AddModel {
    
    public void add(student s)
    {
        try
        { 
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/records?user=root&password=grace@50");
            PreparedStatement p = conn.prepareStatement("Insert Into student (studentID, surname, `first name`, `admission year`, GPA, program) VALUES "
                    + "(?,?,?,?,?,?)");
            p.setString(1, s.studentID);
            p.setString(2, s.surname);
            p.setString(3, s.firstName);
            p.setInt(4, s.admissionYear);
            p.setFloat(5, s.GPA);
            p.setString(6, s.getMajor());
            p.execute();
            System.out.println("Done inserting");
            p.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
}

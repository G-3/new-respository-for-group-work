/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentrecord.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import studentrecord.model.AddModel;
import studentrecord.model.DeleteModel;
import studentrecord.model.EditModel;
import studentrecord.model.MyTableModel;
import studentrecord.view.AddNewForm;
import studentrecord.view.DeleteView;
import studentrecord.view.EditView;
import studentrecord.view.TableView;
import studentrecord.view.recordView;

/**
 *
 * @author Mac-Noble
 */
public class RecordViewController implements ActionListener {
    recordView view;
    public RecordViewController(recordView r) {
        view = r;
        view.setVisible(true);
    }
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getExitButton() )
        {               
            view.dispose();
        }
        if (ae.getSource() == view.getAddButton())
        {  
            new AddNewFormController(new AddModel(), new AddNewForm()).control();
        }
        if (ae.getSource() == view.getViewButton())
        {  
            new TableViewController(new MyTableModel(), new TableView()).control();
        }
        if (ae.getSource() == view.getDeleteButton())
        { 
            new DeleteViewController(new DeleteModel(), new DeleteView()).control();
        }
        if (ae.getSource() == view.getEditButton())
        { 
            new EditViewController(new EditView(), new EditModel()).control();
        }
    }
    public void control(){
        view.getAddButton().addActionListener(this);
        view.getExitButton().addActionListener(this);
        view.getViewButton().addActionListener(this);
        view.getDeleteButton().addActionListener(this);
        view.getEditButton().addActionListener(this);
    }
}

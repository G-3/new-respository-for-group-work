/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentrecord.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import studentrecord.model.MyTableModel;
import studentrecord.view.TableView;

/**
 *
 * @author Samuel Gu
 */
public class TableViewController implements ActionListener {
    TableView view;
    MyTableModel model;
    public TableViewController(MyTableModel m, TableView r) {
        model = m;
        view = r;
        view.getTable().setModel(model);
        view.setVisible(true);
    }
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getExitButton() )
        {               
            view.dispose();
        }
    }
    public void control(){
        view.getExitButton().addActionListener(this);
    }
    
}

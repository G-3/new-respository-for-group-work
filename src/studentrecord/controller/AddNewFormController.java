/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentrecord.controller;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import studentrecord.model.AddModel;
import studentrecord.model.student;
import studentrecord.view.AddNewForm;

/**
 *
 * @author Mac-Noble
 */
public class AddNewFormController implements ActionListener {
    AddNewForm view; 
    AddModel model;
    //I have not been consistent in view & model naming. 
    //should be addNewForm etc.
    public AddNewFormController(AddModel addModel, AddNewForm addNewForm) {
        view = addNewForm;
        view.setVisible(true);
        model = addModel;        
    }
    public void control(){
        view.getAddButton().addActionListener(this);
        view.getCloseButton().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getCloseButton() )
        {
            view.dispose();
        }
        if (ae.getSource() == view.getAddButton())
        {  
            String id = view.getID();
            String su = view.getSurname();
            String f = view.getFirstName();
            int ad = view.getAdmissionYear();
            float g = view.getGPA();
            String m = view.getMajor();
            student s = new student(id, su, f, ad, g, m);
            model.add(s);  
            System.out.println("Done add student");
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentrecord.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import studentrecord.model.DeleteModel;
import studentrecord.view.DeleteView;

/**
 *
 * @author Mac-Noble
 */
public class DeleteViewController  implements ActionListener{
    DeleteView view; 
    DeleteModel model;
    //I have not been consistent in view & model naming. 
    //should be addNewForm etc.
    public DeleteViewController(DeleteModel dModel, DeleteView addDeleteForm) {
        view = addDeleteForm;
        view.setVisible(true);
        model = dModel;        
    }
    public void control(){
        view.getDeleteBtn().addActionListener(this);
        //view.getCloseButton().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getDeleteBtn())
        {  
           if(model.delete(view.getIdToDelete()))
               JOptionPane.showMessageDialog(null, "Done deleting student with ID: " + view.getIdToDelete());
        }
    }
}

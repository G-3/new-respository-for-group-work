/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentrecord.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import studentrecord.model.AddModel;
import studentrecord.model.EditModel;
import studentrecord.model.student;
import studentrecord.view.AddNewForm;
import studentrecord.view.EditView;

/**
 *
 * @author Life without stress
 */
public class EditViewController implements ActionListener {
    EditView view; 
    EditModel model;
    String id = null;
    //I have not been consistent in view & model naming. 
    //should be addNewForm etc.
    public EditViewController(EditView edit, EditModel m) {
        view = edit;
        view.setVisible(true);
        view.getFirstPane2().setVisible(false);
        model = m;
    }
    public void control(){
        view.getEditBtn().addActionListener(this);
        view.getSelectIDBtn().addActionListener(this);
        view.getCloseButton().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {     
        if (ae.getSource() == view.getSelectIDBtn())
        {  
            view.setVisible(true);
            id = view.getEditID();
            view.getFirstPanel().setVisible(false);
            view.getFirstPane2().setVisible(true);
        }
         if (ae.getSource() == view.getEditBtn())
        { 
            String su = view.getSurname();
            String f = view.getFirstName();
            int ad = view.getAdmissionYear();
            float g = view.getGPA();
            String m = view.getMajor();
            student s = new student(id, su, f, ad, g, m);
            model.edit(s);
        }
         if (ae.getSource() == view.getCloseButton() )
        {
            view.dispose();
        }
    }
}
